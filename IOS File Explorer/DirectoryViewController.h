//
//  DirectoryViewController.h
//  IOS File Explorer
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectoryViewController : UITableViewController
@property (nonatomic,readwrite,strong) NSString * path;
@property (nonatomic,readwrite,strong) NSArray * contents;
@end
