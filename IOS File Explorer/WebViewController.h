//
//  WebViewController.h
//  IOS File Explorer
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSData * data;
@property (nonatomic) NSString * mimeType;

@end
