//
//  WebViewController.m
//  IOS File Explorer
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "WebViewController.h"
@interface WebViewController() 
@end
@implementation WebViewController
- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.webView loadData:self.data MIMEType:self.mimeType textEncodingName:@"UTF-8" baseURL:nil];
}
@end
