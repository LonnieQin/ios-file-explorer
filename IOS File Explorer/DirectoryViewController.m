//
//  DirectoryViewController.m
//  IOS File Explorer
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DirectoryViewController.h"
#import "WebViewController.h"
@import MobileCoreServices;
@implementation DirectoryViewController
- (void) setPath:(NSString *)path
{
    _path = path;
    [self setTitle:[path lastPathComponent]];
}

- (void) loadContents
{
    if (self.contents == nil) {
        if (self.path == nil) {
            self.path = @"/";
        }
        NSFileManager * fileManager = [NSFileManager new];
        NSError * error;
        self.contents = [fileManager contentsOfDirectoryAtPath:self.path error:&error];
        
        if (error) {
            NSLog(@"Error:%@",error);
        }
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self loadContents];
    return self.contents.count;
}

- (BOOL) isDirectoryAtEntryPath:(NSString*) entryPath
{
    BOOL isDirectory;
    NSFileManager * fm = [NSFileManager new];
    NSString * fullPath = [self.path stringByAppendingPathComponent:entryPath];
    [fm fileExistsAtPath:fullPath isDirectory:&isDirectory];
    return isDirectory;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self loadContents];
    static NSString * cellIdentifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NSString * entryPath = [self.contents objectAtIndex:indexPath.row];
    NSFileManager * fm = [NSFileManager new];
    NSError * error;
    NSDictionary * attributes = [fm attributesOfItemAtPath:entryPath error:&error];
    
    if (!attributes) {
        NSLog(@"Couldn't get attribute3s for %@: %@",entryPath,error);
    }
    
    NSLog(@"%@=%@",entryPath,attributes);
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[entryPath lastPathComponent]];
    if ([self isDirectoryAtEntryPath:entryPath]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    
    
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * entryPath = [self.contents objectAtIndex:indexPath.row];
    NSString * fullpath = [self.path stringByAppendingPathComponent:[self.contents objectAtIndex:[self.tableView indexPathForSelectedRow].row]];
    if ([self isDirectoryAtEntryPath:entryPath]) {
        DirectoryViewController * directoryViewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"directory"];
        directoryViewVC.path = fullpath;
        [self.navigationController pushViewController:directoryViewVC animated:YES];
    } else {
        NSError * error;
        NSData * data = [NSData dataWithContentsOfFile:fullpath];
        CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[fullpath pathExtension], NULL);
        CFShow(UTI);
        CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
        NSString  *mimeType = (__bridge id)MIMEType;
        NSString * str = [[NSString alloc] initWithContentsOfFile:fullpath encoding:NSUTF8StringEncoding error:nil];
        if (data && mimeType) {
             [self performSegueWithIdentifier:@"web" sender:@{@"data":data,@"mimetype":mimeType}];
        } else if (str != nil) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:entryPath message:str delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
            [alert show];
        }
        else if (error) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"错误码:%d",error.code] message:error.localizedDescription delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
            [alert show];
        }
    }

}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"web"]) {
        WebViewController * controller = segue.destinationViewController;
        NSDictionary * info = sender;
        controller.data = info[@"data"];
        controller.mimeType = info[@"mimetype"];  
    }
}


@end
